Source: mupen64plus-core
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Sven Eckelmann <sven@narfation.org>,
 Tobias Loose <TobiasLoose@gmx.de>,
Standards-Version: 4.7.2
Rules-Requires-Root: no
Homepage: https://mupen64plus.org/
Vcs-Git: https://salsa.debian.org/games-team/mupen64plus-core.git -b debian/unstable
Vcs-Browser: https://salsa.debian.org/games-team/mupen64plus-core
Build-Depends:
 debhelper-compat (= 13),
 libfreetype-dev,
 libgl-dev [any-i386 any-amd64],
 libglu1-mesa-dev [any-i386 any-amd64] | libglu-dev [any-i386 any-amd64],
 libminizip-dev,
 libopencv-dev (>= 4),
 libpng-dev,
 libsdl2-dev,
 libsdl2-net-dev,
 libvulkan-dev,
 nasm,
 pkgconf,
 zlib1g-dev | libz-dev,

Package: libmupen64plus-dev
Section: libdevel
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: mupen64plus development package
 Flexible N64 Emulator for Linux that works with many ROMs that are publically
 available. It consists of separate components to provide an user interface,
 audio and graphics output, controller input, co-processor emulation and an
 emulator core.
 .
 This package contains everything which is needed to build plugins or frontends
 against the mupen64plus API.

Package: libmupen64plus2
Section: libs
Architecture: any-amd64 any-i386
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 mupen64plus-data,
 fonts-dejavu-core,
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 mupen64plus-audio-abi-2,
 mupen64plus-config-abi-2.2,
 mupen64plus-config-abi-2.3,
 mupen64plus-config-abi-2.3.1,
 mupen64plus-config-abi-2.3.2,
 mupen64plus-debug-abi-2,
 mupen64plus-debug-abi-2.0.1,
 mupen64plus-frontend-abi-2.1.1,
 mupen64plus-frontend-abi-2.1.2,
 mupen64plus-frontend-abi-2.1.3,
 mupen64plus-frontend-abi-2.1.4,
 mupen64plus-frontend-abi-2.1.5,
 mupen64plus-frontend-abi-2.1.6,
 mupen64plus-gfx-abi-2.1,
 mupen64plus-gfx-abi-2.2,
 mupen64plus-input-abi-2.1,
 mupen64plus-input-abi-2.1.1,
 mupen64plus-netplay-abi-1.0.1,
 mupen64plus-rsp-abi-2,
 mupen64plus-vidext-abi-3,
 mupen64plus-vidext-abi-3.1,
 mupen64plus-vidext-abi-3.2,
 mupen64plus-vidext-abi-3.3,
Description: plugin-based Nintendo 64 emulator, core library
 Flexible N64 Emulator for Linux that works with many ROMs that are publically
 available. It consists of separate components to provide an user interface,
 audio and graphics output, controller input, co-processor emulation and an
 emulator core.
 .
 It has the following features:
  * cheat system
  * exchangeable plugins for graphics, sound and input emulation
  * multiple save states
  * common configuration system for all components
 .
 This package contains the actual emulator which must be loaded by a frontend.

Package: mupen64plus-data
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Breaks:
 libmupen64plus2 (<< 1.99.5-3),
 mupen64plus (<< 1.99.1),
Replaces:
 libmupen64plus2 (<< 1.99.5-3),
 mupen64plus (<< 1.99.1),
Description: plugin-based Nintendo 64 emulator, data files
 Flexible N64 Emulator for Linux that works with many ROMs that are publically
 available. It consists of separate components to provide an user interface,
 audio and graphics output, controller input, co-processor emulation and an
 emulator core.
 .
 It has the following features:
  * cheat system
  * exchangeable plugins for graphics, sound and input emulation
  * multiple save states
  * common configuration system for all components
 .
 This package contains the rom catalog and cheat database.
